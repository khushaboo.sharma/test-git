<?php

namespace App\Services\Authentication\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Domains\Authentication\Jobs\ReturnViewTemplateJob;

class LoginViewFeature extends Feature
{
    public function handle(Request $request)
    {
      
           return $this->run(new ReturnViewTemplateJob('authentication::login'));
      
    }
}
