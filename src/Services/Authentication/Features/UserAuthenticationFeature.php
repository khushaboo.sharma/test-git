<?php

namespace App\Services\Authentication\Features;

use Lucid\Foundation\Feature;
use Illuminate\Http\Request;
use App\Domains\User\Jobs\ValidateUserJob;
use Illuminate\Support\Facades\Session;


class UserAuthenticationFeature extends Feature
{
    public function handle(Request $request)
    {
        $user = $this->run(new ValidateUserJob($request));

        if(empty($user['error-code'])){
           dd("Login Successfully");
           
           
        } else {
            $request->session()->flash('errorMessage', $user['message']);
            
           return redirect()->route('authentication.login');
        }
       

    }
}
