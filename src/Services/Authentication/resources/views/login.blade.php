<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato', 'Helvetica';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
       
        <div class="container">
            <div class="content">
            @if(Session::has('errorMessage'))
        
                <label class="error">{{ Session::get('errorMessage') }}</label>
                @endif
                {{ Form::open(array('url' => route('authenticate'), 'method' => 'post')) }}
                @csrf
                <div class="form-group">
                    {{Form::email('email',NULL,['class' => 'form-control', 'placeholder' => 'Please Enter Email Address'])}}
                   
                  
                </div>
                <div class="form-group">
                    {{Form::password('password',NULL,['class' => 'form-control', 'placeholder' => 'Please Enter Password'])}}
                   
                </div>
                <div class="form-group">
                    {{Form::submit('submit')}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </body>
</html>
