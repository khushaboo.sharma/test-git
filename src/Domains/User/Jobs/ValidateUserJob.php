<?php

namespace App\Domains\User\Jobs;

use Lucid\Foundation\Job;
use Hash;

class ValidateUserJob extends Job
{
    protected $requestData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->requestData = $requestData->all();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $email = $this->requestData['email'];
       $password = $this->requestData['password'];
       $user = array('email' => 'test@gmail.com','password' => '$2y$10$4MDdeqbjc8Y7mB0CcSjW4.TI6eJMQX1qlHB7gcChNH9ZtYnq.ocb2');
       if($email = $user['email'] && Hash::check($password, $user['password'])){
         $return = $user;
       }else{
         $return =  [
            'error-code' => 'invalidCredentials',
            'message' => 'Invalid User Credentials',
         ];
       }
       return $return;
    }
}
