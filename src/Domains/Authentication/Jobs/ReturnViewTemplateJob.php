<?php

namespace App\Domains\Authentication\Jobs;

use Lucid\Foundation\Job;
use Illuminate\Routing\ResponseFactory;

class ReturnViewTemplateJob extends Job
{
    protected $template;
    protected $data;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($template, $data = [])
    {
        $this->template = $template;
        $this->data = $data;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ResponseFactory $factory)
    {
        return $factory->view($this->template,$this->data);
    }
}
