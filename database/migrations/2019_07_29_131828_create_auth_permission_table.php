<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_permission', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150);
            $table->bigInteger('created_by');
            $table->dateTime('created_on');
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_on')->nullable();
            $table->enum('is_deleted',['0','1','2'])->default('0')->comment('0 => active, 1 => incative, 2 => deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_permission');
    }
}
