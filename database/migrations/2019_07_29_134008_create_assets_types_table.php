<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets_types', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name',30)->comment('hold the assets type name like Job, User profile');
            $table->enum('status',['0','1','2'])->default('0')->comment('0 => active, 1 => incative, 2 => deleted');
            $table->dateTime('created_on');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets_types');
    }
}
