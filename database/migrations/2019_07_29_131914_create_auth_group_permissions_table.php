<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthGroupPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_group_permissions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('group_id')->unsigned()->index()->comment('foreign key of auth group');
            $table->foreign('group_id')->references('id')->on('auth_group')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('permission_id')->unsigned()->index()->comment('foreign key of permission table');
            $table->foreign('permission_id')->references('id')->on('auth_permission')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('created_by');
            $table->dateTime('created_on');
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_on')->nullable();
            $table->enum('is_deleted',['0','1','2'])->default('0')->comment('0 => active, 1 => incative, 2 => deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_group_permissions');
    }
}
