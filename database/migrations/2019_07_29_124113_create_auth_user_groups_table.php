<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_user_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned()->index()->comment('foreign key of internal user table primary key');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('group_id')->unsigned()->index()->comment('foreign key of auth group table primary key');
            $table->foreign('group_id')->references('id')->on('auth_group')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('created_by');
            $table->dateTime('created_on');
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_on')->nullable();
            $table->enum('status',['0','1','2'])->default('0')->comment('0 => active, 1 => incative, 2 => deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_user_groups');
    }
}
