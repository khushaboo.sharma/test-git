<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('item_id')->comment('foreign key of job, users, organization etc, it will have multiple table relations');
            $table->string('url',512)->default('')->comment('Hold asset url and extra information');
            $table->smallInteger('asset_type_id')->unsigned()->index()->comment('foreign key of assets types table');
            $table->foreign('asset_type_id')->references('id')->on('assets_types')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('status',['0','1','2'])->default('0')->comment('0 => active, 1 => incative, 2 => deleted');
            $table->bigInteger('created_by');
            $table->dateTime('created_on');
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_on')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
