<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username')->length(100)->nullable()->comment('hold the username for only internal users');
            $table->string('email')->length(150)->unique();
            $table->string('first_name')->length(50)->default('');
            $table->string('last_name')->length(50)->default('');
            $table->string('password')->length(100)->default('');
            $table->string('salt')->length(100)->default('')->comment('hold the password salt');
            $table->string('algo')->length(150)->default('')->comment('hold the password algorithm');
            $table->string('reset_pass_key')->length(100)->nullable()->comment('hold the forget password key of user');
            $table->dateTime('reset_pass_key_time')->nullable()->comment('hold the forget password date and time to expire');
            $table->decimal('latitude',15,0)->nullable()->comment('hold the latitude of user');
            $table->decimal('longitude',15,0)->nullable()->comment('hold the longtitude');
            $table->string('mobile')->length(20)->default('');
            $table->enum('gender', ['1','2','3'])->default('1')->comment('1 => Male, 2 => Female, 3 => Other');
            $table->date('dob')->nullable();
            $table->integer('user_type')->default('0')->comment('0 => Candidate, 1 => Hire,2 => CompanyOwner, 3 => SuperAdmin');
            $table->integer('signup_status')->default('0')->comment('0 => pending, 1 => completed');
            $table->smallInteger('hire_request')->default('0')->comment('0 => candidate,1 => hire');
            $table->tinyInteger('is_approved')->default('0')->comment('0 => not approved, 1 => approved');
            $table->integer('location_id')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0 => active, 1 => incative, 3 => deleted');
            $table->string('verification_code')->length(11)->default('')->comment('for verify the user based on email address');
            $table->bigInteger('created_by')->comment('hold the user creator id');
            $table->dateTime('created_on')->comment('hold the user created date');
            $table->bigInteger('updated_by')->nullable()->comment('hold the user updated by');
            $table->timestamp('updated_on')->nullable()->comment('hold the user updated date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
