<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSignupFromTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_signup_from', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index()->comment('foreign key of internal user table primary key');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('social_media_id',255)->comment('hold the user social media unique id user wise');
            $table->smallInteger('social_media_type_id')->unsigned()->index()->comment('foreign key of social media table');
            $table->foreign('social_media_type_id')->references('id')->on('social_media_types')->onDelete('cascade')->onUpdate('cascade');
            $table->string('access_token',255)->default('')->comment('hold the access_token of social media, it will be updated on every login');
            $table->enum('status',['0','1','2'])->default('0')->comment('0 => active, 1 => incative, 2 => deleted');
            $table->dateTime('created_on');
            $table->timestamp('updated_on')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_signup_from');
    }
}
