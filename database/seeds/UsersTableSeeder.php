<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dt = Carbon::now();
        $dateNow = $dt->toDateTimeString();
        DB::table('users')->insert([
            'email' => 'admin@texcom.com',
            'password'   => bcrypt('Texcom@123'),
            'user_type' => 3,
            'signup_status' => 1,
            'is_approved' => '1',
            'created_by' => 1,
            'created_on' => $dateNow
        ]);
    }
}
